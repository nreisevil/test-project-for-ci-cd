pipeline {
    agent any

    environment {
        dockerimagename = "hello-world:${env.BUILD_ID}"
        dockerImage = ""
    }

    
    stages {
        stage('Build Docker Image') {
            steps {
                script {
                    dockerImage = docker.build dockerimagename
                }
            }
        }
        
        stage('Push Docker Image') {
            steps {
                script {
                    docker.withRegistry("https://localhost:5000") {
                        dockerImage.push("latest")
                    }
                }
            }
        }
        
        stage('Deploy to Kubernetes') {
            steps {
                script {
                    sh 'kubectl create -f deployment.yaml'
                }
            }
        }
    }
}